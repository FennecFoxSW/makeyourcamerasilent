﻿namespace MakeYourCameraSilent
{
    partial class MakeYourCameraSilent
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.titleLabel = new System.Windows.Forms.Label();
            this.subTitleLabel = new System.Windows.Forms.Label();
            this.exitButton = new System.Windows.Forms.Button();
            this.downloadButton = new System.Windows.Forms.Button();
            this.step1Label = new System.Windows.Forms.Label();
            this.step2Label = new System.Windows.Forms.Label();
            this.enableDevOptionButton = new System.Windows.Forms.Button();
            this.isHaveADBCheckbox = new System.Windows.Forms.CheckBox();
            this.detectDeviceButton = new System.Windows.Forms.Button();
            this.stop3Label = new System.Windows.Forms.Label();
            this.log = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.makeSilentButton = new System.Windows.Forms.Button();
            this.restoreButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.creditLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // titleLabel
            // 
            this.titleLabel.AutoSize = true;
            this.titleLabel.Font = new System.Drawing.Font("Noto Sans KR Light", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleLabel.Location = new System.Drawing.Point(-2, 38);
            this.titleLabel.Name = "titleLabel";
            this.titleLabel.Size = new System.Drawing.Size(344, 43);
            this.titleLabel.TabIndex = 0;
            this.titleLabel.Text = "Make your camera Silent.";
            // 
            // subTitleLabel
            // 
            this.subTitleLabel.AutoSize = true;
            this.subTitleLabel.Font = new System.Drawing.Font("나눔고딕 Light", 11F);
            this.subTitleLabel.Location = new System.Drawing.Point(4, 81);
            this.subTitleLabel.Name = "subTitleLabel";
            this.subTitleLabel.Size = new System.Drawing.Size(180, 18);
            this.subTitleLabel.TabIndex = 2;
            this.subTitleLabel.Text = "카메라를 조용하게 만들기.";
            // 
            // exitButton
            // 
            this.exitButton.Location = new System.Drawing.Point(316, 12);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(22, 23);
            this.exitButton.TabIndex = 3;
            this.exitButton.Text = "X";
            this.exitButton.UseVisualStyleBackColor = true;
            this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
            // 
            // downloadButton
            // 
            this.downloadButton.Font = new System.Drawing.Font("나눔고딕 Light", 10F);
            this.downloadButton.Location = new System.Drawing.Point(6, 179);
            this.downloadButton.Name = "downloadButton";
            this.downloadButton.Size = new System.Drawing.Size(332, 23);
            this.downloadButton.TabIndex = 4;
            this.downloadButton.Text = "ADB 다운로드하기,";
            this.downloadButton.UseVisualStyleBackColor = true;
            this.downloadButton.Click += new System.EventHandler(this.downloadButton_Click);
            // 
            // step1Label
            // 
            this.step1Label.AutoSize = true;
            this.step1Label.Font = new System.Drawing.Font("나눔고딕 Light", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.step1Label.Location = new System.Drawing.Point(3, 153);
            this.step1Label.Name = "step1Label";
            this.step1Label.Size = new System.Drawing.Size(269, 23);
            this.step1Label.TabIndex = 5;
            this.step1Label.Text = "1단계. ADB를 다운로드 하세요.";
            // 
            // step2Label
            // 
            this.step2Label.AutoSize = true;
            this.step2Label.Font = new System.Drawing.Font("나눔고딕 Light", 11F);
            this.step2Label.Location = new System.Drawing.Point(3, 243);
            this.step2Label.Name = "step2Label";
            this.step2Label.Size = new System.Drawing.Size(336, 18);
            this.step2Label.TabIndex = 6;
            this.step2Label.Text = "2단계. 개발자 옵션에서 USB 디버깅을 허용하세요.";
            // 
            // enableDevOptionButton
            // 
            this.enableDevOptionButton.Font = new System.Drawing.Font("나눔고딕 Light", 10F);
            this.enableDevOptionButton.Location = new System.Drawing.Point(6, 264);
            this.enableDevOptionButton.Name = "enableDevOptionButton";
            this.enableDevOptionButton.Size = new System.Drawing.Size(331, 23);
            this.enableDevOptionButton.TabIndex = 7;
            this.enableDevOptionButton.Text = "개발자 옵션 활성화 하는 방법";
            this.enableDevOptionButton.UseVisualStyleBackColor = true;
            this.enableDevOptionButton.Click += new System.EventHandler(this.enableDevOptionButton_Click);
            // 
            // isHaveADBCheckbox
            // 
            this.isHaveADBCheckbox.AutoSize = true;
            this.isHaveADBCheckbox.Font = new System.Drawing.Font("나눔고딕 Light", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.isHaveADBCheckbox.Location = new System.Drawing.Point(6, 209);
            this.isHaveADBCheckbox.Name = "isHaveADBCheckbox";
            this.isHaveADBCheckbox.Size = new System.Drawing.Size(275, 22);
            this.isHaveADBCheckbox.TabIndex = 8;
            this.isHaveADBCheckbox.Text = "환경변수에 ADB가 등록되어 있습니다.";
            this.isHaveADBCheckbox.UseVisualStyleBackColor = true;
            this.isHaveADBCheckbox.CheckedChanged += new System.EventHandler(this.isHaveADBCheckbox_CheckedChanged);
            // 
            // detectDeviceButton
            // 
            this.detectDeviceButton.Font = new System.Drawing.Font("나눔고딕 Light", 10F);
            this.detectDeviceButton.Location = new System.Drawing.Point(6, 332);
            this.detectDeviceButton.Name = "detectDeviceButton";
            this.detectDeviceButton.Size = new System.Drawing.Size(331, 23);
            this.detectDeviceButton.TabIndex = 10;
            this.detectDeviceButton.Text = "디바이스 감지하기.";
            this.detectDeviceButton.UseVisualStyleBackColor = true;
            this.detectDeviceButton.Click += new System.EventHandler(this.detectDeviceButton_Clock);
            // 
            // stop3Label
            // 
            this.stop3Label.AutoSize = true;
            this.stop3Label.Font = new System.Drawing.Font("나눔고딕 Light", 10F);
            this.stop3Label.Location = new System.Drawing.Point(3, 311);
            this.stop3Label.Name = "stop3Label";
            this.stop3Label.Size = new System.Drawing.Size(342, 17);
            this.stop3Label.TabIndex = 9;
            this.stop3Label.Text = "3단계. 디바이스가 정상적으로 연결되었는지 감지합니다.";
            // 
            // log
            // 
            this.log.Location = new System.Drawing.Point(6, 507);
            this.log.Multiline = true;
            this.log.Name = "log";
            this.log.ReadOnly = true;
            this.log.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.log.Size = new System.Drawing.Size(331, 150);
            this.log.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("나눔고딕 Light", 9.75F);
            this.label1.Location = new System.Drawing.Point(3, 358);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(336, 15);
            this.label1.TabIndex = 12;
            this.label1.Text = "List of devices attached아래 뭐가 있으면 감지된것입니다.";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("나눔고딕 Light", 10F);
            this.label2.Location = new System.Drawing.Point(4, 429);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(252, 17);
            this.label2.TabIndex = 13;
            this.label2.Text = "4단계. 카메라를 무음으로 만들어 봅시다.";
            // 
            // makeSilentButton
            // 
            this.makeSilentButton.Font = new System.Drawing.Font("나눔고딕 Light", 10F);
            this.makeSilentButton.Location = new System.Drawing.Point(6, 449);
            this.makeSilentButton.Name = "makeSilentButton";
            this.makeSilentButton.Size = new System.Drawing.Size(331, 23);
            this.makeSilentButton.TabIndex = 14;
            this.makeSilentButton.Text = "무음 만들기.";
            this.makeSilentButton.UseVisualStyleBackColor = true;
            this.makeSilentButton.Click += new System.EventHandler(this.makeSilentButton_Click);
            // 
            // restoreButton
            // 
            this.restoreButton.Font = new System.Drawing.Font("나눔고딕 Light", 10F);
            this.restoreButton.Location = new System.Drawing.Point(6, 478);
            this.restoreButton.Name = "restoreButton";
            this.restoreButton.Size = new System.Drawing.Size(331, 23);
            this.restoreButton.TabIndex = 15;
            this.restoreButton.Text = "원래대로 되돌리기.";
            this.restoreButton.UseVisualStyleBackColor = true;
            this.restoreButton.Click += new System.EventHandler(this.restoreButton_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("나눔고딕 Light", 10F);
            this.label3.Location = new System.Drawing.Point(3, 378);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(212, 17);
            this.label3.TabIndex = 16;
            this.label3.Text = "adb devices 아래 아무것도 없다면";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label4.Font = new System.Drawing.Font("나눔고딕 Light", 10F);
            this.label4.Location = new System.Drawing.Point(4, 397);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(269, 17);
            this.label4.TabIndex = 17;
            this.label4.Text = "다운로드를 다시 하시고, 체크를 해제하세요.";
            // 
            // creditLabel
            // 
            this.creditLabel.AutoSize = true;
            this.creditLabel.Font = new System.Drawing.Font("나눔고딕 Light", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.creditLabel.Location = new System.Drawing.Point(5, 663);
            this.creditLabel.Name = "creditLabel";
            this.creditLabel.Size = new System.Drawing.Size(105, 18);
            this.creditLabel.TabIndex = 18;
            this.creditLabel.Text = "FennecFoxSW";
            // 
            // MakeYourCameraSilent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(350, 688);
            this.ControlBox = false;
            this.Controls.Add(this.creditLabel);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.restoreButton);
            this.Controls.Add(this.makeSilentButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.log);
            this.Controls.Add(this.detectDeviceButton);
            this.Controls.Add(this.stop3Label);
            this.Controls.Add(this.isHaveADBCheckbox);
            this.Controls.Add(this.enableDevOptionButton);
            this.Controls.Add(this.step2Label);
            this.Controls.Add(this.step1Label);
            this.Controls.Add(this.downloadButton);
            this.Controls.Add(this.exitButton);
            this.Controls.Add(this.subTitleLabel);
            this.Controls.Add(this.titleLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MakeYourCameraSilent";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Make Your Camera Silent.";
            this.Load += new System.EventHandler(this.MakeYourCameraSilent_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label titleLabel;
        private System.Windows.Forms.Label subTitleLabel;
        private System.Windows.Forms.Button exitButton;
        private System.Windows.Forms.Button downloadButton;
        private System.Windows.Forms.Label step1Label;
        private System.Windows.Forms.Label step2Label;
        private System.Windows.Forms.Button enableDevOptionButton;
        private System.Windows.Forms.CheckBox isHaveADBCheckbox;
        private System.Windows.Forms.Button detectDeviceButton;
        private System.Windows.Forms.Label stop3Label;
        private System.Windows.Forms.TextBox log;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button makeSilentButton;
        private System.Windows.Forms.Button restoreButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label creditLabel;
    }
}

