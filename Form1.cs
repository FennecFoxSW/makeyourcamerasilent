﻿using System;
using System.Windows.Forms;
using System.IO;
using System.Net;
using System.Diagnostics;
using System.Text;
using System.IO.Compression;

namespace MakeYourCameraSilent
{
    public partial class MakeYourCameraSilent : Form
    {

        private string runCommand(string command)
        {
            ProcessStartInfo cmd = new ProcessStartInfo();
            Process process = new Process();
            cmd.FileName = @"cmd";
            cmd.WindowStyle = ProcessWindowStyle.Hidden;
            cmd.CreateNoWindow = true;
            cmd.UseShellExecute = false;
            cmd.RedirectStandardOutput = true;
            cmd.RedirectStandardInput = true;
            cmd.RedirectStandardError = true;
            process.EnableRaisingEvents = false;
            process.StartInfo = cmd;
            process.Start();
            process.StandardInput.Write(command);
            process.StandardInput.Close();
            string result = process.StandardOutput.ReadToEnd();
            StringBuilder sb = new StringBuilder();
            sb.Append(result);
            process.WaitForExit();
            process.Close();
            return sb.ToString();
        }

        bool isHaveADB = false;
        string tempPath = Path.Combine(Path.GetTempPath());

        public MakeYourCameraSilent()
        {
            InitializeComponent();
        }

        private void MakeYourCameraSilent_Load(object sender, EventArgs e)
        {

        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void downloadButton_Click(object sender, EventArgs e)
        {
            try
            {
                WebClient webClient = new WebClient();
                webClient.DownloadFile("https://dl.google.com/android/repository/platform-tools-latest-windows.zip", tempPath + @"platform-tools-windows.zip");
                ZipFile.ExtractToDirectory(tempPath + @"platform-tools-windows.zip", tempPath);
            }
            catch (IOException e)
            {
                
            }
        }

        private void isHaveADBCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            isHaveADB = (isHaveADBCheckbox.Checked == true) ? true : false;
        }

        private void enableDevOptionButton_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://www.itworld.co.kr/howto/114162");
        }

        private void detectDeviceButton_Clock(object sender, EventArgs e)
        {
            log.Text = "";
            string command = "echo off" + Environment.NewLine + ((isHaveADB == true) ? "" : "cd " + tempPath + "\\platform-tools" + Environment.NewLine) + "adb start-server" + Environment.NewLine + "adb devices" + Environment.NewLine;
            log.Text = runCommand(command);
        }

        private void makeSilentButton_Click(object sender, EventArgs e)
        {
            log.Text = "";
            string command = "echo off" + Environment.NewLine + ((isHaveADB == true) ? "" : "cd " + tempPath + "\\platform-tools" + Environment.NewLine) + "adb shell settings put system csc_pref_camera_forced_shuttersound_key 0" + Environment.NewLine;
            log.Text = runCommand(command);
            MessageBox.Show("카메라 또는 디바이스를 다시 시작해주세요.", "", MessageBoxButtons.OK)
        }

        private void restoreButton_Click(object sender, EventArgs e)
        {
            log.Text = "";
            string command = "echo off" + Environment.NewLine + ((isHaveADB == true) ? "" : "cd " + tempPath + "\\platform-tools" + Environment.NewLine) + "adb shell settings put system csc_pref_camera_forced_shuttersound_key 1" + Environment.NewLine;
            log.Text = runCommand(command);
            MessageBox.Show("카메라 또는 디바이스를 다시 시작해주세요.", "", MessageBoxButtons.OK);
        }
    }
}
